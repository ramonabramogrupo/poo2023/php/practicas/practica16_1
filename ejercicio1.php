<?php

use clases\ejercicio1\Persona;

require_once "autoload.php";
// Crear un objeto de la clase 'Persona' y mostrar su información
$persona1 = new Persona("Juan", 30);
echo $persona1->mostrarInformacion();
