<?php

use clases\ejercicio2\Usuario;

require_once "autoload.php";

// Crear una instancia de Usuario y probar los métodos
$usuario1 = new Usuario(1, "Juan", "juan@alpe.es", 25, true);
echo $usuario1->mostrarInformacion();
$usuario1->activarUsuario();
$usuario1->cambiarEdad(30);
echo $usuario1->mostrarInformacion();
