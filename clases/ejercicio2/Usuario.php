<?php

namespace clases\ejercicio2;

class Usuario
{
    // Propiedades
    public int $idUsuario;
    public string $nombre;
    public string $email;
    public int $edad;
    public bool $activo;

    // Constructor
    public function __construct(int $idUsuario, string $nombre, string $email, int $edad, bool $activo)
    {
        $this->idUsuario = $idUsuario;
        $this->nombre = $nombre;
        $this->email = $email;
        $this->edad = $edad;
        $this->activo = $activo;
    }

    // Métodos
    public function mostrarInformacion(): string
    {
        $salida = "Información del usuario:<br>";
        $salida .= "ID: " . $this->idUsuario . "<br>";
        $salida .= "Nombre:" . $this->nombre . "<br>";
        $salida .= "Email: " . $this->email . "<br>";
        $salida .= "Edad: " . $this->edad . "<br>";
        $salida .= ($this->activo) ? "Activo" : "Inactivo";
        $salida .= "<br>";
        return $salida;
    }

    public function activarUsuario(): void
    {
        $this->activo = true;
    }

    public function cambiarEdad($nuevaEdad): void
    {
        $this->edad = $nuevaEdad;
    }
}
