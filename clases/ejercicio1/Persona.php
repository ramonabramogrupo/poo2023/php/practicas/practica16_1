<?php

namespace clases\ejercicio1;
// Definición de la clase 'Persona'
class Persona
{
    // Atributos
    public string $nombre;
    public int $edad;

    // Constructor
    public function __construct(string $nombre, int $edad)
    {
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    // Método para mostrar información de la persona
    public function mostrarInformacion(): string
    {
        return "Nombre: " . $this->nombre . ", Edad: " . $this->edad . " años";
    }
}
